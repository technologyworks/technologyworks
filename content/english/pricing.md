---
title: "Service Packages"
description: "Our service packages allow clients to book and secure tech services for a determined time period, with defined service priority and cost. Pricing is defined together with our clients according to their business contexts."
bg_image: "images/coachingtraining.jpg"
layout: "pricing"
draft: false

################################ pricing ################################
pricing:
  enable : true
  pricing_table:
  # pricing table loop
  - name : "Free, 30 min"
    price: "Initial Consultation"
    content : ""
    link : "contact"
    services:
    - Introduction to IT services
    - Assessmend of IT & security
    - Determine priorities for next steps
    - Remote, office hours

  # pricing table loop
  - name : "Ask for Quote"
    price: "Coaching & Training"
    content : ""
    link : "contact"
    services:
    - Coaching on Digital Technologies
    - Training on equipment, apps, & services
    - Individual or in groups
    - Remote or on-site, office hours

  # pricing table loop
  - name : "Ask for Quote"
    price: "Tech Support Subscription"
    content : ""
    link : "contact"
    services:
    - Maintenance, support, & incident response
    - Assessments, training, and coaching
    - Further development and improvements
    - Remote or on-site, 24/7 availability

#  # pricing table loop
#  - name : "Ask for Quote"
#    price: "Coaching & Training"
#    content : "For small & medium business enterprises"
#    link : "contact/"
#    services:
#    - Regular maintenance, and support
#    - Assessments, training, and coaching
#    - Further development and improvements
#    - Remote or on-site, 24/7 availability



############################# call to action #################################
cta:
  enable : true
  # call to action content comes from "_index.md"
---
