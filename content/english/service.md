---
title: "Service"
description: "this is meta description"
bg_image: "images/feature-bg.jpg"
layout: "service"
draft: false

########################### about service #############################
about:
  enable : true
  title : "Creative UX/UI Design Agency"
  content : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate soluta corporis odit, optio
          cum! Accusantium numquam ab, natus excepturi architecto earum ipsa aliquam, illum, omnis rerum, eveniet
          officia nihil. Eum quod iure nulla, soluta architecto distinctio. Nesciunt odio ullam expedita, neque fugit
          maiores sunt perferendis placeat autem animi, nihil quis suscipit quibusdam ut reiciendis doloribus natus nemo
          id quod illum aut culpa perspiciatis consequuntur tempore? Facilis nam vitae iure quisquam eius harum
          consequatur sapiente assumenda, officia voluptas quas numquam placeat, alias molestias nisi laudantium
          nesciunt perspiciatis suscipit hic voluptate corporis id distinctio earum. Dolor reprehenderit fuga dolore
          officia adipisci neque!"
  image : "images/company/company-group-pic.jpg"


########################## featured service ############################
featured_service:
  enable : true
  service_item:
    # featured service item loop
    - name : "Interface Design"
      icon : "fas fa-flask"
      color : "primary"
      content : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."

    # featured service item loop
    - name : "Product Branding"
      icon : "fas fa-leaf"
      color : "primary-dark"
      content : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."

    # featured service item loop
    - name : "Game Development"
      icon : "fas fa-lightbulb"
      color : "primary-darker"
      content : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."


############################# Service ###############################
service:
  enable : true
  title : "Our Services"
  description : "We help small & medium enterprises, individuals, and families towards an efficient, secure, and reliable use of digital technologies:"
  service_item:
    # service item loop
    - icon : fab fa-apple #https://fontawesome.com/v5.15/icons
      name: Support for Mac & Apple
      content: "We help clients get the most out of their MacBooks, iMacs, iPads, iPhones, Apple Watches, apps, and services"

    # service item loop
    - icon : fas fa-chalkboard-teacher #https://fontawesome.com/v5.15/icons
      name: Coaching & Training
      content: "Personalised coaching and training supports clients to be more efficient and secure with digital tools & apps"

    # service item loop
    - icon : fas fa-user-shield #https://fontawesome.com/v5.15/icons
      name: IT Security
      content: "We perform structured assessments of IT Security and help clients to implement industry best practices"

    # service item loop
    - icon : fas fa-parachute-box #https://fontawesome.com/v5.15/icons
      name: Business Continuity
      content: "Important parts of life and business are digital. We help enterprises with data protection and business continuity"

    # service item loop
    - icon : fas fa-people-carry #https://fontawesome.com/v5.15/icons
      name: Migrate to Mac & Apple
      content: "A migration to Apple products help clients obtain superior IT security, usability, durability, and reliability"
      
    # service item loop
    - icon : fas fa-store #https://fontawesome.com/v5.15/icons
      name: Web & E-Commerce
      content: "Advice on websites & webshops on designing, building, maintaining, cutting costs and improving agility"

    # service item loop
    - icon : fas fa-piggy-bank #https://fontawesome.com/v5.15/icons
      name: Human Pricing
      content: "Pricing for services are defined together with clients, proportional to budgets and business contexts"

    # service item loop
    - icon : fas fa-laptop-house #https://fontawesome.com/v5.15/icons
      name: On-Site & Remote Support
      content: "Our services are flexible and adapted to small & medium enterprises, individuals, and families"

############################# call to action #################################
cta:
  enable : true
  # call to action content comes from "_index.md"
---
