---
############################### Banner ##############################
banner:
  enable: true
  bg_image: "images/appleproducts.jpg"
  bg_overlay: true
  title: "Technology is Better<br>When IT Works!<br><br>La Tecnologia è Migliore<br>Quando Funziona!<br>"
  content: "Affordable Support for Mac, Apple, Web, & Digital Technologies for Enterprises and Individuals in🇨🇭<br>Assistenza Accessibile per Mac, Apple, Web e tutte le Tecnologie Digitali per Aziende e Privati in Suisse🇨🇭<br>Preiswerter Support für Mac, Apple, Web und digitale Technologien für Firmen und Privatpersonen in🇨🇭"
  button:
    enable: true
    label: "Contact Us"
    link: "contact"

############################# About #################################
about:
  enable: true
  title: "Tech Support for Humans"
  description: "Personalised support and training to get the best of:<br><b>&bull;</b> Mac, Apple devices, and cloud services<br><b>&bull;</b> Improvements in IT security / cybersecurity<br><b>&bull;</b> Digital technologies for business.<br><br>We help enterprises and individuals on all digital fronts to make technology work better! We also help cut costs and boost websites, e-commerce, apps, cloud services, and digital collaboration. Based in Ticino🇨🇭, providing both remote and on-site services."
  image: "images/deskdomore.jpg"


######################### Portfolio ###############################
portfolio:
  enable: true
  bg_image: "images/feature-bg.jpg"
  title: "Get to the Next Level with Mac & Apple"
  content: "We help our clients to:<br><br><b>&bull;</b> Get the most out of MacBooks, iMacs, iPads, iPhones, Apple Watches, & apps<br><b>&bull;</b> Migrate to Apple systems, that are superior in security, usability, and reliability<br><b>&bull;</b> Improve IT Security / Cybersecurity, because our lives depend on digital tech.<br><br>"
  button:
    enable: true
    label: "Contact us for a dialogue on solutions and a quote"
    link: "contact"


############################# Service ############################
service:
  enable: true
  # service content comes from "service.md" file


############################ call to action ###########################
cta:
  enable: true
  bg_image: "images/coachingtraining.jpg"
  title: "Contact us for Personalised IT Services"
  content: "Through personalised support, coaching, and training we help clients to solve problems, increase efficiency, and improve security. <br>Both on-site and remote support are available, according to the client´s needs. We operate across Continental & Northern Europe."
  button:
    enable: true
    label: "Contact Us"
    link: "contact"

############################# Funfacts ###############################
funfacts:
  enable: true
  title: "Facts About Us"
  description: "'We help our clients to improve digital technologies in business and in live. This contributes to increased productivity, security, and reliability. Furthermore, our efforts often are cost-efficient and improve agility. Our track record in industry is excellent on doing both the ordinary and the extraordinary to support the success of our clients.'"
  funfact_item:
  # funfacts item loop
#  - icon: "fas fa-parachute-box" #https://fontawesome.com/v5.0/icons
#    name: "IT Incidents Prevented"
#    count: "64"

  # funfacts item loop
 # - icon: "fas fa-photo-video" #https://fontawesome.com/v5.9/icons
 #   name: "Photos & Videos Rescued"
 #   count: "125000"

  # funfacts item loop
  - icon: "fas fa-briefcase" #https://fontawesome.com/v5.3/icons
    name: "Years in Tech"
    count: "35"

  # funfacts item loop
  - icon: "fas fa-tasks" #https://fontawesome.com/v5.15/icons
    name: "Cybersecurity Controls"
    count: "153"

  # funfacts item loop
#  - icon: "fas fa-laptop" #https://fontawesome.com/v5.15/icons
#    name: "Devices Restored"
#    count: "400"

  # funfacts item loop
  - icon: "fas fa-clock" #https://fontawesome.com/v5.15/icons
    name: "Support Hours per Day"
    count: "24"

  # funfacts item loop
  - icon: "fas fa-calendar" #https://fontawesome.com/v5.15/icons
    name: "Support Days per Week"
    count: "7"

  testimonial_slider:
  # testimonial item loop
  - name: "Arco IT"
    image: "images/clients/arcoit.png"
    designation: ""
    content: "Broad and deep expertise allow him to both develop and implement solutions, with valuable contributions. He demonstrated initiative and high endurance in difficult project situations."

  # testimonial item loop
  - name: "Artex the Factory"
    image: "images/clients/artexthefactory.png"
    designation: ""
    content: "Intelligent, hard working, devoted, structured and achieves results! Outstanding ability to dig into new subjects and draw conclusions that matters. Has also a feel for style and graphic design."

  # testimonial item loop
  - name: "Ellemtel Telecom Labs"
    image: "images/clients/ellemtel.png"
    designation: ""
    content: "Powerful skills in analyzing problems and devising new solutions. Reliable, and a computer expert that gets the job done. Highly recommended for his competence, work ethic, and the value he adds."


  # testimonial item loop
  - name: "TechnologyWorks.ch"
    image: "images/clients/Antonio.Macchi_LinkedIn.jpg"
    designation: "Tech Expert"
    content: "TechnologyWorks.ch is me, an engineer & manager with a passion for digital tech in business and life. Background in industrial product development and research. Expertise in Mac, cloud, web, IT security, Linux, and IoT."


---
